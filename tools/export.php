<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Project\Idea\Vote;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
if (!$USER->isAdmin()) {
    LocalRedirect('/');
}
$request = Application::getInstance()->getContext()->getRequest();

if (Loader::includeModule('project.idea') and $request->get('ID')) {
    Vote::export('php://output', $request->get('ID'));
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename=' . strtolower($_SERVER['SERVER_NAME']) . '.text.csv');
    header('Pragma: no-cache');
}