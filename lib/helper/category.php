<?

namespace Project\Idea\Helper;

use CIdeaManagment,
    Project\Idea\Config;

class Category {

    static public function getById($id) {
        static $arCategoryList = array();
        if (empty($arCategoryList)) {
            CIdeaManagment::getInstance()->Idea()->SetCategoryListId(Config::IBLOCK_CATEGORIES);
            $arCategoryList = CIdeaManagment::getInstance()->Idea()->GetCategoryList();
        }
        return $arCategoryList[$id] ?? false;
    }

}
