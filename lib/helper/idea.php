<?

namespace Project\Idea\Helper;

use CBlogPost,
    CIdeaManagment,
    Bitrix\Main\Loader,
    Project\Idea\Config,
    Project\Idea\Map,
    Tm\Portal\Helper;

class Idea {

    static public function getSobranieList() {
        if (!Loader::includeModule('blog') or ! Loader::includeModule('idea')) {
            return array();
        }

        $arFilter = array(
            'IDEA_STATUS' => Config::STATUS_SOGLASOVANO,
            'UF_STATUS' => Config::STATUS_SOGLASOVANO_ID,
            'PUBLISH_STATUS' => array(
                0 => 'P',
                1 => 'K',
                2 => 'D',
            ),
            'BLOG_ID' => Config::BLOG_ID,
        );
        $dbPost = CBlogPost::GetList(array(), $arFilter, false, false, array('ID', 'AUTHOR_ID', 'TITLE', CIdeaManagment::UFCategroryCodeField));
        $arResult = array(
            'USER' => array_combine(Config::COLLECTION_USER, Config::COLLECTION_USER)
        );
        while ($CurPost = $dbPost->Fetch()) {
            if (empty($arResult['USER'][$CurPost['AUTHOR_ID']])) {
                $arResult['USER'][$CurPost['AUTHOR_ID']] = $CurPost['AUTHOR_ID'];
                $userId = Helper\Deportament::getHeadByUser($CurPost['AUTHOR_ID']);
                $arResult['USER'][$userId] = $userId;
            }
            $code = 'Задачи';
            if (!empty($CurPost[CIdeaManagment::UFCategroryCodeField])) {
                $Category = \Project\Idea\Helper\Category::getById($CurPost[CIdeaManagment::UFCategroryCodeField]);
                $code = trim($Category["NAME"]);
            }
            $arResult['TITLE'][$code][$CurPost['TITLE']] = '<a href="' . Map::getIdeaPost($CurPost['ID']) . '">' . $CurPost['TITLE'] . '</a>';
        }
//        preExit($arResult);
        if (empty($arResult['TITLE'])) {
            return;
        }
        ksort($arResult['TITLE']);
        $idea = array();
        foreach ($arResult['TITLE'] as $category => $ideaList) {
            $idea[] = $category . ':';
            sort($ideaList);
            $idea = array_merge($idea, $ideaList);
        }
        $arResult['IDEA'] = $idea;
        return $arResult;
    }

}
