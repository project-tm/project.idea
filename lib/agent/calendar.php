<?

namespace Project\Idea\Agent;

use cUser,
    CEvent,
    CCalendar,
    CCalendarEvent,
    Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Project\Idea\Config,
    Project\Idea\Helper,
    Tm\Portal\Utility,
    Project\Tools;

class Calendar {

    static public function process() {
        Tools\Utility\User::authorize(1);
        $dateFrom = self::addCalendar();
        self::sendMail($dateFrom);
        Tools\Utility\User::logout();
        return '\Project\Idea\Agent\Calendar::process();';
    }

    static private function sendMail($dateFrom) {
        if (date('d') != date('d', mktime(1, 1, 1, date('n') + 1, 0))) {
            return;
        }
        if (date('H') < 20) {
            return;
        }
        $path = Application::getDocumentRoot() . '/upload/tmp/project.idea/' . date('m');
//        pre($path, file_exists($path));
        if (file_exists($path)) {
            return;
        }
        $arResult = Helper\Idea::getSobranieList();
        foreach ($arResult['USER'] as $userId) {
            $arFields = array(
                "DATE_FROM" => $dateFrom,
                "DESCRIPTION" => nl2br(implode(PHP_EOL, $arResult['IDEA'])),
                "EMAIL_TO" => CUser::GetById($userId)->Fetch()['EMAIL'],
            );
            preExport($arFields);
//            CEvent::Send("IDEA_VSTRECHA", 's1', $arFields);
//            pre($arFields);
        }
        CheckDirPath($path);
        file_put_contents($path, 1);
//        preExit($arFields);
//        exit;
    }

    static private function addCalendar() {
        if (!Loader::includeModule('calendar')) {
            return;
        }

        $noWorkList = Utility\Date::isNoWorkList();
        for ($i = 1; $i < 10; $i++) {
            $time = mktime(Config::COLLECTION_TIME, 0, 0, date('n') + 1, $i);
            if (date('N', $time) > 5) {
                continue;
            }
            if (isset($noWorkList[date('n', $time) - 1][date('j', $time)])) {
                continue;
            }
            break;
        }
        $arResult = Helper\Idea::getSobranieList();
        $param = array(
            'arFields' => array(
                'ID' => 0,
                'DATE_FROM' => date('d.m.Y H:i:s', $time),
                'DATE_TO' => date('d.m.Y H:i:s', $time + 60 * 60),
                'TZ_FROM' => 'Europe/Moscow',
                'TZ_TO' => 'Europe/Moscow',
                'NAME' => 'Обсуждение идей',
                'DESCRIPTION' => 'Идеи на портале.' . PHP_EOL . implode(PHP_EOL, $arResult['IDEA']),
                'SECTIONS' => array(
                    Config::SECTIONS
                ),
                'COLOR' => '',
                'TEXT_COLOR' => '',
                'ACCESSIBILITY' => 'busy',
                'IMPORTANCE' => 'normal',
                'PRIVATE_EVENT' => false,
                'RRULE' => false,
                'LOCATION' => array(
                    'OLD' => 'false',
                    'NEW' => 'ECMR_4703',
                ),
                'REMIND' => array(
                    array(
                        'type' => 'min',
                        'count' => 15,
                    ),
                ),
                'IS_MEETING' => true,
                'SKIP_TIME' => false,
                'ATTENDEES_CODES' => array(),
                'ATTENDEES' => array(),
                'MEETING_HOST' => '1',
                'MEETING' => array(
                    'HOST_NAME' => '',
                    'TEXT' => '',
                    'OPEN' => false,
                    'NOTIFY' => false,
                    'REINVITE' => false,
                ),
            ),
            'UF' => array(
                'UF_WEBDAV_CAL_EVENT' => array(),
                'UF_CRM_CAL_EVENT' => array(),
            ),
            'silentErrorMode' => false,
            'recursionEditMode' => '',
            'currentEventDateFrom' => '01.01.1970',
        );
        foreach ($arResult['USER'] as $value) {
            $param['arFields']['ATTENDEES_CODES'][] = 'U' . $value;
            $param['arFields']['ATTENDEES'][] = $value;
        }

        $arFilter = array(
            'arFilter' =>
            array(
                'NAME' => $param['arFields']['NAME'],
                'FROM_LIMIT' => date('d.m.Y', $time),
                'TO_LIMIT' => date('d.m.Y', $time),
                'SECTIONS' => $param['arFields']['SECTIONS']
            )
        );
        $result = CCalendarEvent::GetList($arFilter);
//        pre($result[0]['ID']);
//        preExit($result[0]['DESCRIPTION']);
        if ($result) {
            $param['arFields']['ID'] = $result[0]['ID'];
            $param['arFields']['DATE_FROM'] = $result[0]['DATE_FROM'];
            $param['arFields']['DATE_TO'] = $result[0]['DATE_TO'];
        }
        CCalendar::SaveEvent($param);
        return $param['arFields']['DATE_FROM'];
    }

}
