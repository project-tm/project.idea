<?

namespace Project\Idea\Agent;

use CBlogPost,
    CIMMessenger,
    CIdeaManagment,
    Bitrix\Main\Loader,
    Bitrix\Main\Type\DateTime,
    Project\Idea\Config,
    Project\Idea\Helper;

class Vote {

    static public function process() {
        self::agent();
        return '\Project\Idea\Agent\Vote::process();';
    }

    static private function agent() {
        if (!Loader::includeModule('blog') or ! Loader::includeModule('socialnetwork') or ! Loader::includeModule('vote') or ! Loader::includeModule('im')) {
            return;
        }
        $arFilter = array(
            'IDEA_STATUS' => Config::STATUS_PROCESSING,
            'UF_STATUS' => Config::STATUS_PROCESSING_ID,
            '!UF_POST_VOTE' => 0,
            'PUBLISH_STATUS' => array(
                0 => 'P',
                1 => 'K',
                2 => 'D',
            ),
            'BLOG_ID' => Config::BLOG_ID,
        );
//        pre($arFilter);
        $arVote = array();
        $dbPost = CBlogPost::GetList(array(), $arFilter, false, false, array('ID', 'TITLE', 'UF_POST_VOTE', 'AUTHOR_ID', CIdeaManagment::UFCategroryCodeField));
        while ($CurPost = $dbPost->Fetch()) {
            Helper\Category::getById($id);
            $arVote[$CurPost['UF_POST_VOTE']] = $CurPost;
        }
        if (empty($arVote)) {
            return;
        }

        $arFilter = array(
            'ID' => array_keys($arVote)
        );

        $dbPost = CBlogPost::GetList(array(), $arFilter, false, false, array('ID', 'ACTIVE', 'BLOG_ID', 'TITLE', 'UF_BLOG_POST_VOTE', 'DATE_PUBLISH'));
        while ($CurPost = $dbPost->Fetch()) {
//            pre($CurPost, $arVote[$CurPost['ID']]);
            $itemVote = $arVote[$CurPost['ID']];
            $ID = $itemVote['ID'];
            $objDateTime = new DateTime($CurPost["DATE_PUBLISH"]);
            $objDateTime->add(Config::VOTE_TIME_PROCESSING); 
            if ($objDateTime->getTimestamp() < time()) {
                list(, $vote) = \Bitrix\Vote\Attach::getData($CurPost["UF_BLOG_POST_VOTE"]);
                $result = 0;
                foreach ($vote['QUESTIONS'] as $value) {
                    foreach ($value['ANSWERS'] as $value) {
                        if ($value['MESSAGE'] == Config::VOTE_SETTING['ANSWERS'][0]) {
                            $result += $value['COUNTER'];
                        } elseif ($value['MESSAGE'] == Config::VOTE_SETTING['ANSWERS'][1]) {
                            $result -= $value['COUNTER'];
                        }
                    }
                }
                if (empty($result)) {
                    \Bitrix\Vote\VoteTable::update($vote['ID'], array("DATE_END" => new \Bitrix\Main\Type\DateTime(date($format, (time() + 24 * 3600)), $format)));
                } else {
                    $imSend = array(
                        'TITLE' => 'За идею проголосовали',
                        'MESSAGE' => (Helper\Category::getById($itemVote[CIdeaManagment::UFCategroryCodeField])['NAME'] ?? '') . ': ' . $itemVote['TITLE'],
                        'TO_USER_ID' => \Tm\Portal\Helper\Deportament::getHeadByUser($itemVote['AUTHOR_ID']),
                        'FROM_USER_ID' => $itemVote['AUTHOR_ID'],
                        'MESSAGE_TYPE' => 'S', # P - private chat, G - group chat, S - notification
                        'NOTIFY_MODULE' => 'intranet',
                        'NOTIFY_TYPE' => 2, # 1 - confirm, 2 - notify single from, 4 - notify single
                    );
                    if ($result > 0) {
                        $imSend['TITLE'] = 'За идею проголосовали';
                        $arUpdate = array(
                            'UF_STATUS' => Config::VOTE_STATUS['COMPLETED'],
                            'UF_IDEA_RESULT' => Config::VOTE_RESULT['Progolosovali']
                        );
                    } else {
                        $imSend['TITLE'] = 'Проголосовали против идеи';
                        $arUpdate = array(
                            'UF_STATUS' => Config::VOTE_STATUS['CANCEL'],
                            'UF_IDEA_RESULT' => Config::VOTE_RESULT['Ne_progolosovali']
                        );
                    }
                    $imSend['MESSAGE'] = $imSend['TITLE'] . PHP_EOL . $imSend['MESSAGE'];
                    CIMMessenger::Add($imSend);
                    global $USER_FIELD_MANAGER;
//                    pre('BLOG_POST', $ID, $arUpdate);
                    CBlogPost::Update($ID, $arUpdate);
                    \Bitrix\Vote\VoteTable::update($vote['ID'], array("DATE_END" => new \Bitrix\Main\Type\DateTime(date($format, (time() - 3600)), $format)));
//                    pre($result);
                }
            }
            exit;
        }
    }

}
